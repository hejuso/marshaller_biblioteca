package parser.biblioteca;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class Marshaller {

	private Document dom = null;
	private ArrayList<Libro> libros = null;

	public Marshaller(ArrayList<Libro> lib) {
		libros = lib;
	}

	public void crearDocumento() {
		// Creamos una factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {
			// Se crea un document builder
			DocumentBuilder db = dbf.newDocumentBuilder();

			// Se parsea el xml y obtenemos el DOM
			dom = db.newDocument();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	public void crearArbolDom() {
		// creamos el elemento raiz "libros"
		Element docEle = dom.createElement("libros");
		dom.appendChild(docEle);

		// Recorremos
		Iterator it = libros.iterator();
		while (it.hasNext()) {
			Libro lib = (Libro) it.next();
			// Para cada objeto libro, creamos elemento <libro>
			Element libroEle = setLibro(lib);
			docEle.appendChild(libroEle);
		}
	}

	private Element setLibro(Libro lib) {
		// creamos el elemento libro
		Element LibroEle = dom.createElement("libro");

		// creamos el elemento titulo y el a�o y el nodo de texto y integer
		Element tituloEle = dom.createElement("titulo");
		Text tituloTexto = dom.createTextNode(lib.getTitulo());
		tituloEle.setAttribute("anyo", Integer.toString(lib.getAnyo()));
		tituloEle.appendChild(tituloTexto);
		LibroEle.appendChild(tituloEle);

		// creamos el elemento editor y el nodo de valor string
		Element editorEle = dom.createElement("editor");
		Text editorTexto = dom.createTextNode(lib.getEditor());
		editorEle.appendChild(editorTexto);
		LibroEle.appendChild(editorEle);

		// creamos el elemento a�o y el nodo de valor entero
		Element paginasEle = dom.createElement("paginas");
		Text paginasTexto = dom.createTextNode(Integer.toString(lib.getPaginas()));
		paginasEle.appendChild(paginasTexto);
		LibroEle.appendChild(paginasEle);

		// creamos el elemento autores
		Element autorEle = dom.createElement("autor");

		ArrayList<String> autores = lib.getAutor();

		for (Iterator iterator = autores.iterator(); iterator.hasNext();) {
			String autor = (String) iterator.next();
			Element hijo = dom.createElement("nombre");
			hijo.setTextContent(autor);
			autorEle.appendChild(hijo);
			System.out.println(autorEle);

		}

		LibroEle.appendChild(autorEle);

		return LibroEle;
	}

	public void escribirDocumentAXml(File file) throws TransformerException {

		// Creamos una instancia para escribir el resultado
		Transformer trans = TransformerFactory.newInstance().newTransformer();
		trans.setOutputProperty(OutputKeys.INDENT, "yes");

		// Especificamos donde escribimos y la fuente de datos
		StreamResult result = new StreamResult(file);
		DOMSource source = new DOMSource(dom);
		trans.transform(source, result);

	}

}
