package parser.biblioteca;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.text.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

public class Parser_Biblioteca {

	public static void main(String[] args) {

		// Declaramos las variables de los libros
		int anyo = 0;
		int paginas = 0;

		ArrayList<Libro> libros = new ArrayList<Libro>();
		ArrayList<String> autores = new ArrayList<String>();

		String nameFile = null;
		Parser parser = new Parser();
		parser.parseFicheroXml("biblioteca.xml");

		System.out.println("Bienvenido a la biblioteca. �Que operaci�n desea realizar?");
		System.out.println("1- Parsear un fichero.");
		System.out.println("2- Serializar un fichero.");
		Scanner introd = new Scanner(System.in);

		while (!introd.hasNextInt()) {
			introd.next();
			System.out.println("Prueba otra vez");
		}

		int num_int = introd.nextInt();

		switch (num_int) {
		case 1:
			parser.parseDocument();
			parser.print();
			break;
		case 2:
			// Preguntamos cuantos libros se quieren agregar
			System.out.println("Cuantos libros quieres agregar?");
			Scanner num_libros = new Scanner(System.in);
			while (!num_libros.hasNextInt()) {
				num_libros.next();
				System.out.println("Prueba otra vez");
			}
			int num_libros_int = num_libros.nextInt();

			for (int i = 1; i <= num_libros_int; i++) {

				// Titulo del libro
				System.out.println("Introduce el t�tulo del libro");
				Scanner titulo = new Scanner(System.in);
				while (!titulo.hasNext()) {
					titulo.next();
					System.out.println("Prueba otra vez");
				}
				String titulo_str = titulo.nextLine();
				// Autor/es del libro
				System.out.println("Cuantos autores tiene el libro?");
				Scanner num_autores = new Scanner(System.in);
				while (!num_autores.hasNextInt()) {
					num_autores.next();
					System.out.println("Prueba con un n�mero");
				}
				int num_autores_int = num_autores.nextInt();

				for (int j = 1; j <= num_autores_int; j++) {
					System.out.println("Introduzca el nombre del autor " + j);
					Scanner autorn = new Scanner(System.in);
					while (!autorn.hasNext()) {
						autorn.next();
						System.out.println("Prueba otra vez");
					}
					String autor_string = autorn.nextLine();
					autores.add(autor_string);
				}

				// Anyo del libro
				System.out.println("Introduzca el a�o del libro");
				Scanner anyo_int = new Scanner(System.in);
				while (!anyo_int.hasNextInt()) {
					anyo_int.next();
					System.out.println("Prueba con un n�mero");
				}
				anyo = anyo_int.nextInt();

				// Editor del libro
				System.out.println("Introduzca el editor del libro");

				Scanner editor = new Scanner(System.in);
				while (!editor.hasNext()) {
					editor.next();
					System.out.println("Prueba otra vez");
				}
				String editor_str = editor.nextLine();

				// Paginas del libro
				System.out.println("Introduzca las paginas del libro");

				Scanner paginas_int = new Scanner(System.in);
				while (!paginas_int.hasNextInt()) {
					paginas_int.next();
					System.out.println("Prueba con un n�mero");
				}
				paginas = paginas_int.nextInt();

				libros.add(new Libro(titulo_str, autores, anyo, editor_str, paginas));

			}

			Marshaller marshaller = new Marshaller(libros);

			marshaller.crearDocumento();
			marshaller.crearArbolDom();

			System.out.println("Nombre del fichero");
			Scanner fichero = new Scanner(System.in);
			while (!fichero.hasNext()) {
				fichero.next();
				System.out.println("Prueba otra vez");
			}
			nameFile = fichero.nextLine();

			File file = new File(nameFile + ".xml");

			try {
				marshaller.escribirDocumentAXml(file);
			} catch (TransformerException e) {
				e.printStackTrace();
			}

			System.out.println("Fichero serializado");

			break;
		}

	}

}
